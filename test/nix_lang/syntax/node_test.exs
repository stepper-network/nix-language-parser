defmodule NixLang.Syntax.NodeTest do
  use ExUnit.Case, async: true
  use NixLang.Syntax

  describe "guards" do
    test "is_node" do
      assert false == Node.is_node({})
    end
  end

  describe "Node module helper" do
    test "type?/2" do
      assert true == Node.type?(N.root([]), :root)
      assert false == Node.type?(N.root([]), :interpolation)
    end

    test "add_child/2" do
      assert N.root([T.dot()]) = Node.add_child(N.root([]), T.dot())
      assert N.root([T.whitespace(" ")]) = Node.add_child(N.root([]), T.whitespace(" "))
      assert N.root([N.interpolation([])]) = Node.add_child(N.root([]), N.interpolation([]))
    end

    test "add_children/2" do
      assert N.root([]) = Node.add_children(N.root([]), [])
      assert N.root([T.whitespace(" ")]) = Node.add_children(N.root([]), [T.whitespace(" ")])

      assert N.root([T.whitespace(" "), N.interpolation([])]) =
               Node.add_children(N.root([]), [T.whitespace(" "), N.interpolation([])])
    end

    test "finalize/1" do
      assert N.root([]) = Node.finalize(N.root([]))

      assert N.root([N.attribute([T.identifier("a")])]) =
               Node.finalize(N.root([N.attribute([T.identifier("a")])]))

      assert N.root([
               N.attribute([T.identifier("a"), T.whitespace(" ")]),
               N.attribute([T.identifier("b")])
             ]) =
               Node.finalize(
                 N.root([
                   N.attribute([T.identifier("b")]),
                   N.attribute([T.whitespace(" "), T.identifier("a")])
                 ])
               )
    end
  end
end
