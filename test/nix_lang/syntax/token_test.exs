defmodule NixLang.Syntax.TokenTest do
  use ExUnit.Case, async: true
  use NixLang.Syntax.Token

  describe "guards" do
    test "is_token" do
      assert false == Token.is_token({})
      assert true == Token.is_token(T.left_paren())
    end
  end

  describe "Token module helper" do
  end
end
