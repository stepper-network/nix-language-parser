defmodule NixLang.TokenizerCoverageTest do
  use ExUnit.Case, async: true
  use NixLang.Syntax.Token

  describe "guards" do
    require NixLang.Tokenizer

    alias NixLang.Tokenizer

    test "is_string_context/1" do
      assert false == Tokenizer.is_string_context([])
      assert true == Tokenizer.is_string_context([:string])
      assert true == Tokenizer.is_string_context([:indented_string])
      assert true == Tokenizer.is_string_context([:string, :comment])
      assert false == Tokenizer.is_string_context([:comment, :string])
      assert false == Tokenizer.is_string_context([:test])
    end

    test "hd_identifier_grapheme/1" do
      assert false == Tokenizer.hd_identifier_grapheme([])
      assert true == Tokenizer.hd_identifier_grapheme(["a"])
      assert true == Tokenizer.hd_identifier_grapheme(["a", "b"])
      assert false == Tokenizer.hd_identifier_grapheme(["%"])
      assert false == Tokenizer.hd_identifier_grapheme(["%", "a"])
    end

    test "hd_identifier_token/1" do
      assert false == Tokenizer.hd_identifier_token([])
      assert true == Tokenizer.hd_identifier_token([{:identifier, "a"}])
      assert true == Tokenizer.hd_identifier_token([{:identifier, "a"}, {:identifier, "b"}])
      assert false == Tokenizer.hd_identifier_token([{:semicolon, ";"}])
      assert false == Tokenizer.hd_identifier_token([{:semicolon, ";"}, {:identifier, "a"}])
    end

    test "is_keyword/2" do
      assert true == Tokenizer.is_keyword([], [])
      assert true == Tokenizer.is_keyword(["%"], [])
      assert true == Tokenizer.is_keyword([], [{:semicolon, ";"}])
      assert true == Tokenizer.is_keyword(["%"], [{:semicolon, ";"}])
      assert false == Tokenizer.is_keyword(["a"], [])
      assert false == Tokenizer.is_keyword([], [{:identifier, "a"}])
      assert false == Tokenizer.is_keyword(["a"], [{:identifier, "a"}])
    end
  end

  describe "tokenize/1" do
    import NixLang.Tokenizer

    test "empty" do
      assert [] = tokenize("")
    end

    test "whitespace" do
      assert [T.whitespace(" ")] = tokenize(" ")
      assert [T.whitespace(" \n ")] = tokenize(" \n ")
      assert [T.whitespace("\t\n\r ")] = tokenize("\t\n\r ")
    end

    test "single-line comment" do
      assert [T.comment("# test")] = tokenize("# test")

      assert [T.whitespace("\n  "), T.comment("# test "), T.whitespace("\n")] =
               tokenize("\n  # test \n")
    end

    test "multi-line comment" do
      assert [T.comment("/**/")] = tokenize("/**/")
      assert [T.error("/** ")] = tokenize("/** ")
      assert [T.error("/**\n")] = tokenize("/**\n")

      assert [T.comment("/* test\n  test ... test\n*/"), T.whitespace("\n")] =
               tokenize(~S"""
               /* test
                 test ... test
               */
               """)
    end

    test "URI" do
      assert [T.identifier("_"), T.colon(), T.identifier("null")] = tokenize("_:null")

      assert [T.identifier("_"), T.colon(), T.update(), T.identifier("null")] =
               tokenize("_://null")

      assert [T.uri("http://test.com/?q=a+B")] = tokenize("http://test.com/?q=a+B")
      assert [T.uri("http://test_.com/?q=a+B")] = tokenize("http://test_.com/?q=a+B")

      assert [T.uri("http://test.com/"), T.whitespace(" "), T.identifier("a")] =
               tokenize("http://test.com/ a")
    end

    test "standard string" do
      assert [T.string_start(), T.string_end()] = tokenize(~S|""|)
      assert [T.string_start(), T.string_content("test"), T.string_end()] = tokenize(~S|"test"|)

      assert [T.string_start(), T.string_content("hello \\\"world\\\""), T.string_end()] =
               tokenize(~S|"hello \"world\""|)

      assert [T.string_start(), T.string_content("$${test}"), T.string_end()] =
               tokenize(~S|"$${test}"|)

      assert [
               T.string_start(),
               T.string_content("\\$"),
               T.interpolation_start(),
               T.identifier("test"),
               T.interpolation_end(),
               T.string_end()
             ] = tokenize(~S|"\$${test}"|)
    end

    test "indented string" do
      assert [T.indented_string_start(), T.indented_string_end()] = tokenize("''''")

      assert [T.indented_string_start(), T.string_content("test"), T.indented_string_end()] =
               tokenize("''test''")

      assert [
               T.indented_string_start(),
               T.string_content("hello \"world\""),
               T.indented_string_end()
             ] = tokenize(~S|''hello "world"''|)

      assert [T.indented_string_start(), T.string_content("$${test}"), T.indented_string_end()] =
               tokenize("''$${test}''")

      assert [
               T.indented_string_start(),
               T.string_content("''$"),
               T.interpolation_start(),
               T.identifier(test),
               T.interpolation_end(),
               T.indented_string_end()
             ] = tokenize("''''$${test}''")
    end

    test "ellipsis" do
      assert [T.ellipsis()] = tokenize("...")
      assert [T.ellipsis(), T.whitespace(" ")] = tokenize("... ")
    end

    test "path" do
      assert [T.divide()] = tokenize("/")
      assert [T.path("/a")] = tokenize("/a")
      assert [T.path("/a"), T.semicolon()] = tokenize("/a;")
      assert [T.error("/a/")] = tokenize("/a/")
      assert [T.error("/a/"), T.semicolon()] = tokenize("/a/;")

      assert [T.dot(), T.divide()] = tokenize("./")
      assert [T.path("./a")] = tokenize("./a")
      assert [T.error("./a/")] = tokenize("./a/")

      assert [T.error("~")] = tokenize("~")
      assert [T.error("~"), T.semicolon()] = tokenize("~;")
      assert [T.error("~/")] = tokenize("~/")
      assert [T.path("~/a")] = tokenize("~/a")
      assert [T.error("~/a/")] = tokenize("~/a/")

      assert [T.dot(), T.dot(), T.divide()] = tokenize("../")
      assert [T.path("../a")] = tokenize("../a")
      assert [T.error("../a/")] = tokenize("../a/")
    end

    test "store path" do
      assert [T.path("</>")] = tokenize("</>")
      assert [T.path("<nixpkgs>")] = tokenize("<nixpkgs>")
      assert [T.less_than(), T.greater_than()] = tokenize("<>")
      assert [T.less_than(), T.whitespace(" "), T.greater_than()] = tokenize("< >")

      assert [T.less_than(), T.divide(), T.whitespace(" "), T.greater_than()] = tokenize("</ >")
    end

    test "numbers" do
      assert [T.integer("1234")] = tokenize("1234")
      assert [T.float("12.34")] = tokenize("12.34")
      assert [T.float("12.34e5")] = tokenize("12.34e5")
      assert [T.float("12.34E5")] = tokenize("12.34E5")
      assert [T.error("12.34E"), T.whitespace(" ")] = tokenize("12.34E ")
    end

    test "concat" do
      assert [T.identifier("a"), T.concat(), T.identifier("b")] = tokenize("a++b")

      assert [
               T.left_bracket(),
               T.integer("1"),
               T.right_bracket(),
               T.whitespace(" "),
               T.concat(),
               T.whitespace(" "),
               T.left_bracket(),
               T.integer("2"),
               T.right_bracket(),
               T.whitespace(" \n")
             ] = tokenize("[1] ++ [2] \n")
    end

    test "operators" do
      assert [T.add()] = tokenize("+")
      assert [T.subtract()] = tokenize("-")
      assert [T.multiply()] = tokenize("*")
      assert [T.divide()] = tokenize("/")
      assert [T.equal()] = tokenize("==")
      assert [T.not_equal()] = tokenize("!=")
      assert [T.greater_than()] = tokenize(">")
      assert [T.greater_than_or_eq()] = tokenize(">=")
      assert [T.less_than()] = tokenize("<")
      assert [T.less_than_or_eq()] = tokenize("<=")
      assert [T.logical_and()] = tokenize("&&")
      assert [T.logical_or()] = tokenize("||")
      assert [T.invert()] = tokenize("!")
      assert [T.implication()] = tokenize("->")
      assert [T.concat()] = tokenize("++")
      assert [T.update()] = tokenize("//")
      assert [T.left_paren()] = tokenize("(")
      assert [T.right_paren()] = tokenize(")")
    end

    test "underscore" do
      assert [T.identifier("_"), T.colon(), T.identifier("null")] = tokenize("_:null")
    end

    test "symbols" do
      assert [T.semicolon()] = tokenize(";")
      assert [T.assign()] = tokenize("=")
      assert [T.at()] = tokenize("@")
      assert [T.colon()] = tokenize(":")
      assert [T.comma()] = tokenize(",")
      assert [T.dot()] = tokenize(".")
      assert [T.ellipsis()] = tokenize("...")
      assert [T.question_mark()] = tokenize("?")
      assert [T.left_brace()] = tokenize("{")
      assert [T.right_brace()] = tokenize("}")
      assert [T.left_bracket()] = tokenize("[")
      assert [T.right_bracket()] = tokenize("]")
    end

    test "keywords" do
      assert [T.let()] = tokenize("let")
      assert [T.identifier("letter")] = tokenize("letter")
      assert [T.in()] = tokenize("in")
      assert [T.inherit()] = tokenize("inherit")
      assert [T.then()] = tokenize("then")
      assert [T.if()] = tokenize("if")
      assert [T.else()] = tokenize("else")
      assert [T.assert()] = tokenize("assert")
      assert [T.or()] = tokenize("or")
      assert [T.rec()] = tokenize("rec")
      assert [T.with()] = tokenize("with")
    end

    test "identifiers" do
      assert [T.identifier("a")] = tokenize("a")
      assert [T.identifier("_a")] = tokenize("_a")
      assert [T.identifier("ab")] = tokenize("ab")
      assert [T.identifier("a_b")] = tokenize("a_b")
      assert [T.identifier("a"), T.whitespace(" "), T.identifier("b")] = tokenize("a b")
    end

    test "fallthrough for characters ourside of Nix language" do
      assert [T.identifier("hello"), T.error("🐧")] = tokenize("hello🐧")
      assert [T.error("🐧🐧")] = tokenize("🐧🐧")
    end
  end
end
