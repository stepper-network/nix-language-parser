defmodule NixLang.TokenizerErrorTest do
  use ExUnit.Case, async: true
  use NixLang.Syntax.Token
  import NixLang.Tokenizer

  test "path with interpolation error trailing slash" do
    assert [
             T.path("./"),
             T.interpolation_start(),
             T.identifier("foo"),
             T.interpolation_end(),
             T.error("/")
           ] = tokenize("./${foo}/")
  end
end
