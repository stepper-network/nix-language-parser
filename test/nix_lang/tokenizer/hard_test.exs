defmodule NixLang.TokenizerHardTest do
  use ExUnit.Case, async: true
  use NixLang.Syntax.Token
  import NixLang.Tokenizer

  test "interspered comments" do
    assert [
             T.left_brace(),
             T.whitespace(" "),
             T.identifier("a"),
             T.whitespace(" "),
             T.assign(),
             T.whitespace(" "),
             T.comment("/* multiline * comment */"),
             T.whitespace(" "),
             T.integer("123"),
             T.semicolon(),
             T.comment("# single line"),
             T.whitespace("\n"),
             T.right_brace(),
             T.whitespace(" "),
             T.comment("# single line at the end"),
             T.whitespace("\n")
           ] =
             tokenize(~S"""
             { a = /* multiline * comment */ 123;# single line
             } # single line at the end
             """)
  end

  test "division looks like path" do
    assert [
             T.identifier("a"),
             T.dot(),
             T.interpolation_start(),
             T.identifier("foo"),
             T.interpolation_end(),
             T.divide(),
             T.identifier("b"),
             T.dot(),
             T.interpolation_start(),
             T.identifier("bar"),
             T.interpolation_end()
           ] = tokenize("a.${foo}/b.${bar}")
  end

  test "multiple interpolated path" do
    assert [
             T.path("./a."),
             T.interpolation_start(),
             T.identifier("foo"),
             T.interpolation_end(),
             T.path("/b."),
             T.interpolation_start(),
             T.identifier("bar"),
             T.interpolation_end()
           ] = tokenize("./a.${foo}/b.${bar}")
  end

  test "lambda expression" do
    assert [
             T.identifier("a"),
             T.colon(),
             T.whitespace(" "),
             T.identifier("b"),
             T.colon(),
             T.whitespace(" "),
             T.identifier("a"),
             T.whitespace(" "),
             T.add(),
             T.whitespace(" "),
             T.identifier("b")
           ] = tokenize("a: b: a + b")
  end

  test "let path expression" do
    assert [
             T.let(),
             T.whitespace(" "),
             T.identifier("a"),
             T.whitespace(" "),
             T.assign(),
             T.whitespace(" "),
             T.integer("3"),
             T.semicolon(),
             T.whitespace(" "),
             T.in(),
             T.whitespace(" "),
             T.path("/"),
             T.interpolation_start(),
             T.identifier("a"),
             T.interpolation_end(),
             T.path("/b")
           ] = tokenize("let a = 3; in /${a}/b")
  end

  test "string looks like path" do
    assert [
             T.string_start(),
             T.string_content("./"),
             T.interpolation_start(),
             T.identifier("foo"),
             T.interpolation_end(),
             T.string_end()
           ] = tokenize(~S|"./${foo}"|)
  end

  test "string interpretation select" do
    assert [
             T.string_start(),
             T.string_content("Hello, "),
             T.interpolation_start(),
             T.whitespace(" "),
             T.left_brace(),
             T.whitespace(" "),
             T.identifier("world"),
             T.whitespace(" "),
             T.assign(),
             T.whitespace(" "),
             T.string_start(),
             T.string_content("World"),
             T.string_end(),
             T.semicolon(),
             T.whitespace(" "),
             T.right_brace(),
             T.dot(),
             T.identifier("world"),
             T.whitespace(" "),
             T.interpolation_end(),
             T.string_content("!"),
             T.string_end()
           ] = tokenize(~S|"Hello, ${ { world = "World"; }.world }!"|)
  end

  test "nested string interpretation" do
    assert [
             T.indented_string_start(),
             T.interpolation_start(),
             T.string_start(),
             T.interpolation_start(),
             T.identifier(var),
             T.interpolation_end(),
             T.string_end(),
             T.interpolation_end(),
             T.indented_string_end()
           ] = tokenize(~S|''${"${var}"}''|)
  end

  test "dynamic (interpolated) select" do
    assert [
             T.identifier("a"),
             T.dot(),
             T.interpolation_start(),
             T.identifier("b"),
             T.interpolation_end(),
             T.dot(),
             T.identifier("c")
           ] = tokenize("a.${b}.c")
  end

  test "@ pattern" do
    assert [
             T.left_brace(),
             T.whitespace(" "),
             T.identifier("a"),
             T.comma(),
             T.whitespace(" "),
             T.identifier("b"),
             T.whitespace(" "),
             T.question_mark(),
             T.whitespace(" "),
             T.string_start(),
             T.string_content("default"),
             T.string_end(),
             T.comma(),
             T.whitespace(" "),
             T.ellipsis(),
             T.whitespace(" "),
             T.right_brace(),
             T.whitespace(" "),
             T.at(),
             T.whitespace(" "),
             T.identifier("outer")
           ] = tokenize(~S|{ a, b ? "default", ... } @ outer|)
  end

  test "math no whitespace" do
    assert [
             T.integer("5"),
             T.multiply(),
             T.subtract(),
             T.left_paren(),
             T.integer("3"),
             T.subtract(),
             T.integer("2"),
             T.right_paren()
           ] = tokenize("5*-(3-2)")
  end
end
