defmodule NixLang.SimpleTest do
  use ExUnit.Case, async: true
  use NixLang.Syntax
  import NixLang

  test "basic NixOS configuration file" do
    assert {N.root([
              N.function([
                N.function_arg_set([
                  T.left_brace(),
                  T.whitespace(" "),
                  N.attribute([T.identifier("config")]),
                  T.comma(),
                  T.whitespace(" "),
                  N.attribute([T.identifier("pkgs")]),
                  T.comma(),
                  T.whitespace(" "),
                  N.attribute([T.ellipsis(), T.whitespace(" ")]),
                  T.right_brace()
                ]),
                T.colon(),
                T.whitespace("\n\n"),
                N.attribute_set([
                  T.left_brace(),
                  T.whitespace("\n"),
                  N.attribute_assign([
                    N.attribute([T.identifier("imports"), T.whitespace(" ")]),
                    T.assign(),
                    T.whitespace(" "),
                    N.list([
                      T.left_bracket(),
                      T.whitespace(" "),
                      N.path([T.path("./hardware-configuration.nix"), T.whitespace(" ")]),
                      T.right_bracket()
                    ]),
                    T.semicolon(),
                    T.whitespace("\n\n")
                  ]),
                  N.attribute_assign([
                    N.attribute_path([
                      N.attribute([T.identifier("boot")]),
                      T.dot(),
                      N.attribute([T.identifier("loader")]),
                      T.dot(),
                      N.attribute([T.identifier("grub")]),
                      T.dot(),
                      N.attribute([T.identifier("enable"), T.whitespace(" ")])
                    ]),
                    T.assign(),
                    T.whitespace(" "),
                    N.literal([T.boolean("true")]),
                    T.semicolon(),
                    T.whitespace("\n")
                  ]),
                  N.attribute_assign([
                    N.attribute_path([
                      N.attribute([T.identifier("networking")]),
                      T.dot(),
                      N.attribute([T.identifier("useDHCP"), T.whitespace(" ")])
                    ]),
                    T.assign(),
                    T.whitespace(" "),
                    N.literal([T.boolean("false")]),
                    T.semicolon(),
                    T.whitespace("\n\n")
                  ]),
                  N.attribute_assign([
                    N.attribute_path([
                      N.attribute([T.identifier("system")]),
                      T.dot(),
                      N.attribute([T.identifier("stateVersion"), T.whitespace(" ")])
                    ]),
                    T.assign(),
                    T.whitespace(" "),
                    N.string([T.string_start(), T.string_content("21.05"), T.string_end()]),
                    T.semicolon(),
                    T.whitespace("\n")
                  ]),
                  T.right_brace(),
                  T.whitespace("\n")
                ])
              ])
            ]),
            []} =
             parse_nix(~S"""
             { config, pkgs, ... }:

             {
             imports = [ ./hardware-configuration.nix ];

             boot.loader.grub.enable = true;
             networking.useDHCP = false;

             system.stateVersion = "21.05";
             }
             """)
  end
end
