defmodule NixLang.ParserCoverageTest do
  use ExUnit.Case, async: true
  use NixLang.Syntax
  import NixLang.Parser

  describe "guards" do
  end

  describe "parse/1" do
    test "empty tokens" do
      assert {N.root([]), []} = parse([])
    end

    test ":string node parse" do
      assert {N.root([
                N.string([
                  T.string_start(),
                  T.string_content("a"),
                  T.string_end()
                ])
              ]), []} = parse([T.string_start(), T.string_content("a"), T.string_end()])
    end

    test ~S|interpolation ${a}| do
      assert {N.root([
                N.interpolation([
                  T.interpolation_start(),
                  N.attribute([T.identifier("a")]),
                  T.interpolation_end()
                ])
              ]),
              []} =
               parse([
                 T.interpolation_start(),
                 T.identifier("a"),
                 T.interpolation_end()
               ])
    end

    test ~S|attribute a\n| do
      assert {N.root([
                N.attribute([
                  T.identifier("a"),
                  T.whitespace("\n")
                ])
              ]), []} = parse([T.identifier("a"), T.whitespace("\n")])
    end

    test ~S|attribute path a.b\n| do
      assert {N.root([
                N.attribute_path([
                  N.attribute([T.identifier("a")]),
                  T.dot(),
                  N.attribute([T.identifier("b"), T.whitespace("\n")])
                ])
              ]),
              []} =
               parse([
                 T.identifier("a"),
                 T.dot(),
                 T.identifier("b"),
                 T.whitespace("\n")
               ])
    end

    test ~S|string attribute path "a".b\n| do
      assert {N.root([
                N.attribute_path([
                  N.string([T.string_start(), T.string_content("a"), T.string_end()]),
                  T.dot(),
                  N.attribute([T.identifier("b"), T.whitespace("\n")])
                ])
              ]),
              []} =
               parse([
                 T.string_start(),
                 T.string_content("a"),
                 T.string_end(),
                 T.dot(),
                 T.identifier("b"),
                 T.whitespace("\n")
               ])
    end

    test ~S|interpolated attribute path ${a}.b\n| do
      assert {N.root([
                N.attribute_path([
                  N.interpolation([
                    T.interpolation_start(),
                    N.attribute([T.identifier("a")]),
                    T.interpolation_end()
                  ]),
                  T.dot(),
                  N.attribute([T.identifier("b"), T.whitespace("\n")])
                ])
              ]),
              []} =
               parse([
                 T.interpolation_start(),
                 T.identifier("a"),
                 T.interpolation_end(),
                 T.dot(),
                 T.identifier("b"),
                 T.whitespace("\n")
               ])
    end

    {
      :node,
      :root,
      [
        {:node, :attribute_set,
         [
           {:token, :left_brace, "{"},
           {:node, :attribute_assign,
            [
              {:node, :attribute, [{:token, :identifier, "a"}]},
              {:token, :assign, "="},
              {:node, :literal, {:token, :integer, "1"}},
              {:token, :semicolon, ";"}
            ]},
           {:token, :right_brace, "}"},
           {:token, :whitespace, "\n"}
         ]}
      ]
    }

    test ~S|attribute set {a=1};\n| do
      assert {N.root([
                N.attribute_set([
                  T.left_brace(),
                  N.attribute_assign([
                    N.attribute([T.identifier("a")]),
                    T.assign(),
                    N.literal([T.integer("1")]),
                    T.semicolon()
                  ]),
                  T.right_brace(),
                  T.whitespace("\n")
                ])
              ]),
              []} =
               parse([
                 T.left_brace(),
                 T.identifier("a"),
                 T.assign(),
                 T.integer("1"),
                 T.semicolon(),
                 T.right_brace(),
                 T.whitespace("\n")
               ])
    end

    test ":rec node parse" do
      assert {N.root([
                N.rec([
                  T.rec(),
                  N.attribute_set([
                    T.left_brace(),
                    N.attribute_assign([
                      N.attribute([T.identifier("a")]),
                      T.assign(),
                      N.literal([T.integer("1")]),
                      T.semicolon()
                    ]),
                    T.right_brace(),
                    T.whitespace("\n")
                  ])
                ])
              ]),
              []} =
               parse([
                 T.rec(),
                 T.left_brace(),
                 T.identifier("a"),
                 T.assign(),
                 T.integer("1"),
                 T.semicolon(),
                 T.right_brace(),
                 T.whitespace("\n")
               ])
    end
  end
end
