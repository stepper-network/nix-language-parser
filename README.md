# NixParser

Parses the Nix language into Beam data structures.

## Resources

The logic for the parser is a mixture of the following resources:

* Nix Lexer: https://github.com/NixOS/nix/blob/master/src/libexpr/lexer.l
* rnix-parser: https://github.com/nix-community/rnix-parser

