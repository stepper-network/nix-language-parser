defmodule NixLang.Syntax.N do
  @moduledoc """
  Defines macros for working with Parser tree nodes (N).
  """
  alias NixLang.Syntax.Node

  for type <- Node.syntax_nodes() do
    defmacro unquote(type)(c) do
      type = unquote(type)
      quote do: {:node, unquote(type), unquote(c)}
    end
  end
end
