defmodule NixLang.Syntax.Node do
  use NixLang.Syntax.Token

  @node_types [
    :root,
    # used for parse and token errors
    :error,
    :assert,
    :parentheses,
    # integer, float, string
    :literal,
    :path,
    :interpolation,
    :string,
    :unary_op,
    :binary_op,
    # let a = 1; in a
    :let_in,
    # let { a = 1; } a
    :let_legacy,
    :function,
    :function_arg,
    :function_arg_set,
    :list,
    # collection of attribute_assigns
    :attribute_set,
    # attr = value
    :attribute_assign,
    # without dot
    :attribute,
    # with dot/s
    :attribute_path,
    :inherit,
    :inherit_from,
    :rec,
    :with,
    :undetermined
  ]

  @type t() :: {:node, node_type(), [t() | Token.t()]}

  @type node_type() ::
          unquote(
            @node_types
            |> Enum.map(&inspect/1)
            |> Enum.join(" | ")
            |> Code.string_to_quoted!()
          )

  defguard is_node(t)
           when is_tuple(t) and tuple_size(t) == 3 and elem(t, 0) == :node and
                  elem(t, 1) in @node_types and is_list(elem(t, 2))

  defmacro __using__(_opts) do
    quote do
      require NixLang.Syntax.N
      alias NixLang.Syntax.{N, Node}
    end
  end

  def type(node) when is_node(node), do: elem(node, 1)
  def children(node) when is_node(node), do: elem(node, 2)

  @spec type?(any, atom | maybe_improper_list) :: boolean
  def type?(node, type) when is_node(node) and is_atom(type),
    do: type(node) == type

  def type?(node, types) when is_node(node) and is_list(types),
    do: Enum.any?(types, fn t -> type?(node, t) end)

  @doc """
  Adds a child to the node.
  """
  @spec add_child(t(), t() | Token.t()) :: t()
  def add_child({:node, type, children}, {:node, _, _} = child),
    do: {:node, type, [child | children]}

  def add_child({:node, type, children}, {:token, _, _} = child),
    do: {:node, type, [child | children]}

  @doc """
  Add multiple children to the node.
  """
  @spec add_children(t(), [t() | Token.t()] | t()) :: t()
  def add_children(node, children) when is_node(node) and is_list(children),
    do: {:node, elem(node, 1), Enum.concat(children, elem(node, 2))}

  @doc """
  Finalizes the Node by reversing the children.

  TODO: Move this out of Node since it handles Tokens as well.
  """
  def finalize({:node, type, children} = node) when is_node(node),
    do: {:node, type, Enum.map(children, &finalize/1) |> Enum.reverse()}

  def finalize(non_node), do: non_node

  @doc """
  Returns a list of the syntax node type atoms.
  """
  @spec syntax_nodes() :: [atom()]
  def syntax_nodes(), do: @node_types
end
