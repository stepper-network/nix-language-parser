defmodule NixLang.Syntax.Token do
  @moduledoc """
  A token in the parsed tree.  These are the actual contents of the Nix source code.
  """

  @static_tokens [
    {:interpolation_start, "${"},
    {:interpolation_end, "}"},
    {:string_start, "\""},
    {:string_end, "\""},
    {:indented_string_start, "''"},
    {:indented_string_end, "''"},
    {:semicolon, ";"},
    {:assign, "="},
    {:at, "@"},
    {:colon, ":"},
    {:comma, ","},
    {:dot, "."},
    {:ellipsis, "..."},
    {:question_mark, "?"},
    {:left_brace, "{"},
    {:right_brace, "}"},
    {:left_bracket, "["},
    {:right_bracket, "]"},
    {:add, "+"},
    {:subtract, "-"},
    {:multiply, "*"},
    {:divide, "/"},
    {:equal, "=="},
    {:not_equal, "!="},
    {:greater_than, ">"},
    {:greater_than_or_eq, ">="},
    {:less_than, "<"},
    {:less_than_or_eq, "<="},
    {:logical_and, "&&"},
    {:logical_or, "||"},
    {:invert, "!"},
    {:implication, "->"},
    {:concat, "++"},
    {:update, "//"},
    {:left_paren, "("},
    {:right_paren, ")"},
    {:let, "let"},
    {:in, "in"},
    {:inherit, "inherit"},
    {:then, "then"},
    {:if, "if"},
    {:else, "else"},
    {:assert, "assert"},
    {:or, "or"},
    {:rec, "rec"},
    {:with, "with"}
  ]

  @variable_tokens [
    :comment,
    :error,
    :whitespace,
    :identifier,
    :boolean,
    :float,
    :integer,
    :path,
    :uri,
    :string_content
  ]

  @token_types @static_tokens
               |> Enum.map(&elem(&1, 0))
               |> Enum.concat(@variable_tokens)

  @type token_type() ::
          unquote(
            @token_types
            |> Enum.map(&inspect/1)
            |> Enum.join(" | ")
            |> Code.string_to_quoted!()
          )

  @type t() :: {:token, token_type(), binary()}

  defmacro __using__(_opts) do
    quote do
      require NixLang.Syntax.T
      require NixLang.Syntax.Token
      alias NixLang.Syntax.{T, Token}
    end
  end

  defguard is_token(t)
           when is_tuple(t) and tuple_size(t) == 3 and elem(t, 0) == :token and
                  elem(t, 1) in @token_types and is_binary(elem(t, 2))

  defguard is_variable(t)
           when is_token(t) and elem(t, 1) in @variable_tokens

  defguard is_trivia(t)
           when is_token(t) and elem(t, 1) in [:comment, :error, :whitespace]

  defguard is_string_start(t)
           when is_token(t) and elem(t, 1) in [:string_start, :indented_string_start]

  defguard is_string(t)
           when is_token(t) and elem(t, 1) in [:string_content, :string_end, :indented_string_end]

  defguard is_literal(t) when is_token(t) and elem(t, 1) in [:boolean, :integer, :float, :uri]

  defguard is_attribute(t) when is_token(t) and elem(t, 1) in [:identifier, :ellipsis]

  @doc """
  Returns a list of static token tuples.
  """
  @spec static_tokens() :: [{atom(), binary()}]
  def static_tokens(), do: @static_tokens

  @doc """
  Returns a list of variable token atoms.
  """
  @spec variable_tokens() :: [atom()]
  def variable_tokens(), do: @variable_tokens

  @spec add(t(), binary()) :: t()
  def add({:token, type, current}, new) when type in @token_types and is_binary(new) do
    {:token, type, current <> new}
  end
end
