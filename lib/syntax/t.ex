defmodule NixLang.Syntax.T do
  @moduledoc """
  Defines macros for working with Parser node tokens (NT).
  """

  alias NixLang.Syntax.Token

  for {type, content} <- Token.static_tokens() do
    defmacro unquote(type)() do
      Macro.escape({:token, unquote(type), unquote(content)})
    end
  end

  for type <- Token.variable_tokens() do
    defmacro unquote(type)(c) do
      type = unquote(type)
      quote do: {:token, unquote(type), unquote(c)}
    end
  end
end
