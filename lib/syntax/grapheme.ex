defmodule NixLang.Syntax.Grapheme do
  @moduledoc """
  Nix language grapheme definitions.

  Only supports a subset of the ASCII printable characters.  Other characters will result in errors
  in the Tokenizer.  This seems to be what Nix supports, but this can be extended easily if that is
  not the case.
  """
  import Kernel, except: [is_number: 1]

  @eol ["\n", "\r"]
  @whitespace Enum.concat([" ", "\t"], @eol)
  # Elixir has `Kernel.is_number/1` so we use the term 'decimal' for the numbers 0..9
  @decimal ~w|0 1 2 3 4 5 6 7 8 9|
  @letter ~w|a b c d e f g h i j k l m n o p q r s t u v w x y z|
          |> Enum.concat(~w|A B C D E F G H I J K L M N O P Q R S T U V W X Y Z|)
  @identifier ["_" | @letter]
  @path ~w|/ . - +|
        |> Enum.concat(@decimal)
        |> Enum.concat(@identifier)
  @uri ~w|% ? : @ & = $ , ! ~ * '|
       |> Enum.concat(@path)

  defguard is_eol(g) when g in @eol
  defguard is_whitespace(g) when g in @whitespace
  defguard is_decimal(g) when g in @decimal
  defguard is_identifier(g) when g in @identifier
  defguard is_path(g) when g in @path
  defguard is_uri(g) when g in @uri

  defmacro __using__(_opts) do
    quote do
      require NixLang.Syntax.Grapheme
      alias NixLang.Syntax.Grapheme
    end
  end

  @spec decimal?(binary()) :: boolean()
  def decimal?(g) when is_decimal(g), do: true
  def decimal?(_), do: false

  @spec identifier?(binary()) :: boolean()
  def identifier?(g) when is_identifier(g), do: true
  def identifier?(_), do: false

  @spec path?(binary()) :: boolean()
  def path?(g) when is_path(g), do: true
  def path?(_), do: false

  @spec uri?(binary()) :: boolean()
  def uri?(g) when is_uri(g), do: true
  def uri?(_), do: false

  @spec whitespace?(binary()) :: boolean()
  def whitespace?(g) when is_whitespace(g), do: true
  def whitespace?(_), do: false
end
