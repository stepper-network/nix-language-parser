defmodule NixLang.Parser do
  use NixLang.Syntax

  @type error() :: binary()

  defguard is_function_arg(n)
           when Node.is_node(n) and elem(n, 1) in [:attribute, :function_arg_set]

  @doc """
  Parses a token list and returns an abstract syntax tree root node and list of errors.
  The errors will be empty on no errors.
  """
  @spec parse([Token.t()]) :: {Node.t(), [error()]}
  def parse(tokens), do: parse(tokens, [], [N.root([])])

  ### Base Cases ###

  @spec parse([Token.t()], [error()], [Node.t()]) :: {Node.t(), [error()]}

  # empty tokens non-root node base case
  defp parse([], errors, [{:node, type, _} = child | [parent | nodes]]) when type != :root,
    do: parse([], errors, [Node.add_child(parent, child) | nodes])

  # empty tokens root node base case
  defp parse([], errors, [N.root(_) = root_node]),
    do: {Node.finalize(root_node), Enum.reverse(errors)}

  ### "Let" Expression ###

  # change :let_in node to :let_legacy
  defp parse([T.left_brace() = token | rest], errors, [N.let_in(children) | nodes]),
    do: parse(rest, errors, [N.let_legacy([token | children]) | nodes])

  # end condition for attribute set for let-in expression
  defp parse([T.in() | _] = tokens, errors, nodes),
    do: adopt_to(tokens, errors, nodes, fn n -> !Node.type?(n, :let_in) end)

  # start let expression for let-in and legacy
  defp parse([T.let() = token | rest], errors, nodes),
    do: parse(rest, errors, [N.let_in([token]) | nodes])

  ### With Expressions ###

  # start with node
  defp parse([{:with, _} = token | rest], errors, nodes),
    do: parse(rest, errors, [N.with([token]) | nodes])

  ### Expression Endings ###

  # end semicolon expression
  defp parse([T.semicolon() | _] = tokens, errors, nodes),
    do: adopt_to(tokens, errors, nodes, fn n -> !Node.type?(n, [:attribute_assign, :with]) end)

  # end rec brace expression
  defp parse([T.right_brace() = token | rest], errors, [
         N.attribute_set(children) | [N.rec(siblings) | nodes]
       ]),
       do: parse(rest, errors, [N.rec([N.attribute_set([token | children]) | siblings]) | nodes])

  # end brace expression
  defp parse([T.right_brace() | _] = tokens, errors, nodes),
    do:
      adopt_to(
        tokens,
        errors,
        nodes,
        fn n -> !Node.type?(n, [:attribute_set, :function_arg_set, :let_legacy]) end
      )

  ### Inherit Expression ###

  # upgrade inherit to inherit_from node
  defp parse([T.left_paren() = token | rest], errors, [N.inherit(_) | _] = nodes),
    do: parse(rest, errors, [N.inherit_from([token]) | nodes])

  # start inhertit node
  defp parse([T.inherit() = token | rest], errors, nodes),
    do: parse(rest, errors, [N.inherit([token]) | nodes])

  ### Parenthesis ###

  # end parentheses expression
  defp parse([T.right_paren() | _] = tokens, errors, nodes),
    do: adopt_to(tokens, errors, nodes, fn n -> !Node.type?(n, :parentheses) end)

  # start parentheses expression
  defp parse([T.left_paren() = token | rest], errors, nodes),
    do: parse(rest, errors, [N.parentheses(token) | nodes])

  ### Attribute Expressions ###

  # dot upgrade to attribute path node
  # this will result in a series of attribute paths before an assign
  defp parse([T.dot() = token | rest], errors, [{:node, node_type, _} = node | nodes])
       when node_type in [:attribute, :interpolation, :string],
       do:
         parse(
           rest,
           errors,
           [N.attribute_path([token, node]) | nodes]
         )

  # dot error case default
  defp parse([T.dot() = token | rest], errors, nodes),
    do: parse(rest, [~S|unexpected "." in tokens| | errors], [N.error([token]) | nodes])

  # start recursive attribute set
  defp parse([T.rec() = token | rest], errors, nodes),
    do: parse(rest, errors, [N.rec([token]) | nodes])

  # start attribute set (fallthrough for left brace)
  defp parse([T.left_brace() = token | rest], errors, nodes),
    do: parse(rest, errors, [N.attribute_set([token]) | nodes])

  # start attribute for any identifier
  defp parse([token | rest], errors, nodes) when Token.is_attribute(token),
    do: parse(rest, errors, [N.attribute([token]) | nodes])

  # start attribute_assign node for attribute path
  defp parse(
         [T.assign() = token | rest],
         errors,
         [N.attribute(_) = attr | [N.attribute_path(_) | _] = nodes]
       ) do
    {paths, nodes} = Enum.split_while(nodes, fn n -> Node.type?(n, :attribute_path) end)
    attr_path = Enum.reduce(paths, [], fn p, acc -> Enum.concat(acc, Node.children(p)) end)

    parse(
      rest,
      errors,
      [N.attribute_assign([token, N.attribute_path([attr | attr_path])]) | nodes]
    )
  end

  # start attribute_assign node for attribute
  defp parse([T.assign() = token | rest], errors, [N.attribute(_) = node | nodes]),
    do: parse(rest, errors, [N.attribute_assign([token, node]) | nodes])

  # attribute_assign error fallthrough
  defp parse([T.assign() = token | rest], errors, [node | nodes]),
    do:
      parse(
        rest,
        ["attribute assign preceded by #{Node.type(node)}" | errors],
        [N.error([token]) | nodes]
      )

  ### List Expressions ###

  # end list
  defp parse([T.right_bracket() | _] = tokens, errors, nodes),
    do: adopt_to(tokens, errors, nodes, fn n -> !Node.type?(n, :list) end)

  # start list
  defp parse([T.left_bracket() = token | rest], errors, nodes),
    do: parse(rest, errors, [N.list([token]) | nodes])

  ### Function Expressions ###

  # upgrade attribute set to function arg set when comma encountered
  defp parse(
         [T.comma() = token | rest],
         errors,
         [N.attribute(_) = child | [N.attribute_set(children) | nodes]]
       ),
       do:
         parse(
           rest,
           errors,
           [N.function_arg_set([token | [child | children]]) | nodes]
         )

  # adopt function args to the set when comma encountered
  defp parse([T.comma() | _] = tokens, errors, nodes),
    do: adopt_to(tokens, errors, nodes, fn n -> !Node.type?(n, :function_arg_set) end)

  # upgrade attribute to function arg
  defp parse([T.colon() = token | rest], errors, [N.attribute(_) = node | nodes]),
    do: parse(rest, errors, [N.function([N.function_arg([token, node])]) | nodes])

  # upgrade attribute set to function arg
  defp parse([T.colon() = token | rest], errors, [N.function_arg_set(_) = node | nodes]),
    do: parse(rest, errors, [N.function([token, node]) | nodes])

  ### Operator Expressions ###
  @binary_op_lhs_types [:uniary_operator, :binary_operator, :literal, :path, :string]

  # addition upgrade to binary operator node
  # defp parse([Tkn.add() = token | rest], errors, [%Node{type: node_type} = node | nodes])
  #      when node_type in @binary_op_lhs_types,
  #      do: parse(rest, errors, [Node.new(:binary_op, [Token.new(token), node]) | nodes])

  # addition error case for incorect left hand side value type
  defp parse([T.add() = token | rest], errors, nodes),
    do:
      parse(
        rest,
        [~S|bad left hand side of "+" binary operation| | errors],
        [N.error([token]) | nodes]
      )

  ### Identifiers, Strings, Paths, and Literals ###

  # continue string node with string content
  defp parse([token | tokens], errors, [N.string(children) | nodes]) when Token.is_string(token),
    do: parse(tokens, errors, [N.string([token | children]) | nodes])

  # add literal node
  defp parse([token | tokens], errors, nodes) when Token.is_literal(token),
    do: parse(tokens, errors, [N.literal([token]) | nodes])

  # add path node
  defp parse([T.path(_) = token | tokens], errors, nodes),
    do: parse(tokens, errors, [N.path([token]) | nodes])

  # add identifier node
  defp parse([T.identifier(_) = token | rest], errors, nodes),
    do: parse(rest, errors, [N.attribute([token]) | nodes])

  # add string node
  defp parse([token | tokens], errors, nodes) when Token.is_string_start(token),
    do: parse(tokens, errors, [N.string([token]) | nodes])

  ### Interpolations ###

  # end interpolated node by wrapping up children
  defp parse([T.interpolation_end() = token | tokens], errors, nodes) do
    with {child_nodes, [N.interpolation(children) | nodes]} <-
           Enum.split_while(nodes, fn n -> !Node.type?(n, :interpolation) end) do
      parse(
        tokens,
        errors,
        [N.interpolation(Enum.concat([token | child_nodes], children)) | nodes]
      )
    end
  end

  # start interpolated node
  defp parse([T.interpolation_start() = token | tokens], errors, nodes),
    do: parse(tokens, errors, [N.interpolation([token]) | nodes])

  ### Parenthesis ###

  # end parentheses node by collecting children
  defp parse([T.right_paren() = token | tokens], errors, nodes) do
    with {children, [N.parentheses(siblings) | nodes]} <-
           Enum.split_while(nodes, fn n -> !Node.type?(n, :parentheses) end) do
      parse(tokens, errors, [N.parentheses(Enum.concat([token | children], siblings)) | nodes])
    end
  end

  # start parenthetic expression
  defp parse([T.left_paren() = token | tokens], errors, nodes),
    do: parse(tokens, errors, [N.parentheses(token) | nodes])

  ### Other Explicit Handling ###

  ### Fallthrough Cases ###

  # add triva to top node
  defp parse([token | rest], errors, [node | nodes]) when Token.is_trivia(token),
    do: parse(rest, errors, [Node.add_child(node, token) | nodes])

  # default case is an error child added to top node
  defp parse([{:token, type, _} = token | rest], errors, [node | nodes]),
    do:
      parse(
        rest,
        [~s|default case for token type: #{type}| | errors],
        [Node.add_child(node, N.error([token])) | nodes]
      )

  ### Helper Functions ###

  # adpots the children to the parent node found with the fun
  defp adopt_to([token | tokens], errors, nodes, fun) do
    case Enum.split_while(nodes, fun) do
      {_, []} ->
        parse(
          tokens,
          ["start node not found for #{elem(token, 0)}" | errors],
          [N.error([token]) | nodes]
        )

      {children, [node | nodes]} ->
        parse(tokens, errors, [Node.add_children(node, [token | children]) | nodes])
    end
  end
end
