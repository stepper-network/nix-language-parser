defmodule NixLang.Tokenizer do
  @moduledoc """
  Tokenizer for the Nix language.  Takes a grapheme binary, converts it to a list of unicode
  graphemes, then converts that to a list of Nix language tokens.  This means that this tokenizer
  can handle arbitrary unicode even if it falls outside of the Nix language (though, outside of
  comments and strings this will result in an `:error` token and ultimately be skipped).

  This tokenizer is implemented with a tail-recursive algorithm.  To help with this, a third
  context stack is used within the internal, recursive functions to aid in interpreting
  the graphemes.  For example, if we detect we are in a comment, we can enter the comment
  context and just add to the comment token until we encounter the end of comment, bypassing
  any further processing.  If we enter an interpolation context within a string, the stack allows
  us to enter and leave the interpolation while still remaining in the string context.

  Note: The order matters for the matching of these functions.  Do not reorder
  without a good reason for doing so.
  """
  use NixLang.Syntax.Grapheme
  use NixLang.Syntax.Token

  defmacro __using__(_opts) do
    quote do
      import unquote(__MODULE__), only: [tokenize: 1]
      require unquote(__MODULE__).T
      alias unquote(__MODULE__).T
    end
  end

  @trivia_token_types [:comment, :error, :whitespace]

  @contexts [
    :comment,
    :multiline_comment,
    :string,
    :indented_string,
    :interpolation,
    :path,
    :uri
  ]

  @string_contexts [:string, :indented_string]

  @path_tokens [
    :add,
    :dot,
    :divide,
    :identifier,
    :subtract
  ]

  defguard is_string_context(contexts)
           when length(contexts) > 0 and hd(contexts) in @string_contexts

  defguard hd_identifier_grapheme(rest) when length(rest) > 0 and Grapheme.is_identifier(hd(rest))

  defguard hd_identifier_token(tokens)
           when length(tokens) > 0 and elem(hd(tokens), 0) == :identifier

  defguard is_keyword(rest, tokens)
           when not hd_identifier_grapheme(rest) and not hd_identifier_token(tokens)

  @spec tokenize(binary()) :: [Token.t()]
  def tokenize(nix) do
    Enum.reverse(tokenize(String.graphemes(nix), [], []))
  end

  ### Internal tokenize/3 tail-recursive function ###

  # base case if there is an unfinished string or multiline-comment context
  defp tokenize([], [{:token, token_type, content} | tokens], context)
       when length(context) > 0 and
              hd(context) in [:string, :indented_string, :multiline_comment] and
              token_type in [:string, :comment],
       do: [T.error(content) | tokens]

  # base case if there is an unfinished path context
  defp tokenize([], [{:token, :path, content} | tokens], [:path | _]),
    do: [upgrade_path_string(content) | tokens]

  # base case
  defp tokenize([], tokens, _context), do: tokens

  ### Comments ###

  # end single-line comment
  defp tokenize([grapheme | rest], [T.comment(_) | _] = tokens, [:comment | context])
       when Grapheme.is_eol(grapheme),
       do: tokenize(rest, [T.whitespace(grapheme) | tokens], context)

  # end multi-line comment
  defp tokenize(
         ["*" | ["/" | rest]],
         [T.comment(_) = token | tokens],
         [
           :multiline_comment | context
         ]
       ),
       do: tokenize(rest, [Token.add(token, "*/") | tokens], context)

  # continue single-line comment
  defp tokenize([grapheme | rest], [T.comment(comment) | tokens], [:comment] = context),
    do: tokenize(rest, [T.comment(comment <> grapheme) | tokens], context)

  # continue multi-line comment
  defp tokenize(
         [grapheme | rest],
         [T.comment(comment) | tokens],
         [:multiline_comment] = context
       ),
       do: tokenize(rest, [T.comment(comment <> grapheme) | tokens], context)

  # one-line comment
  defp tokenize(["#" | rest], tokens, []),
    do: tokenize(rest, [T.comment("#") | tokens], [:comment])

  # multi-line comment
  defp tokenize(["/" | ["*" | rest]], tokens, []),
    do: tokenize(rest, [T.comment("/*") | tokens], [:multiline_comment])

  ### URI Context ###
  # Must happen before interpolation to avoid entering interpolation context

  # end path expression context for other cases
  defp tokenize([grapheme | _] = rest, tokens, [:uri | context])
       when not Grapheme.is_uri(grapheme),
       do: tokenize(rest, tokens, context)

  # continue URI expression context
  defp tokenize([grapheme | rest], [T.uri(uri) | tokens], [:uri | _] = context)
       when Grapheme.is_uri(grapheme),
       do: tokenize(rest, [T.uri(uri <> grapheme) | tokens], context)

  ### String Escaping ###

  # standard string escaping special handing
  defp tokenize(["\\" | [grapheme | rest]], tokens, [:string | _] = context)
       when grapheme in ["$", "\""],
       do: tokenize(rest, add_string_content(tokens, "\\" <> grapheme), context)

  defp tokenize(["$" | ["$" | rest]], tokens, [context | _] = contexts)
       when context in @string_contexts,
       do: tokenize(rest, add_string_content(tokens, "$$"), contexts)

  # indented string escaping special handling
  defp tokenize(["'" | ["'" | [grapheme | rest]]], tokens, [:indented_string | _] = context)
       when grapheme in ["$", "'", "\\"],
       do: tokenize(rest, add_string_content(tokens, "''" <> grapheme), context)

  ### Interpolation ###

  # increment bracket count on left bracket
  defp tokenize(["{" | rest], tokens, [{:interpolation, count} | context]),
    do: tokenize(rest, [T.left_brace() | tokens], [{:interpolation, count + 1} | context])

  # begin interpolation
  defp tokenize(["$" | ["{" | rest]], tokens, context),
    do: tokenize(rest, [T.interpolation_start() | tokens], [{:interpolation, 1} | context])

  # end interpolation
  defp tokenize(["}" | rest], tokens, [{:interpolation, 1} | context]),
    do: tokenize(rest, [T.interpolation_end() | tokens], context)

  # decrement interpolation on right braces
  defp tokenize(["}" | rest], tokens, [{:interpolation, count} | context]),
    do: tokenize(rest, [T.right_brace() | tokens], [{:interpolation, count - 1} | context])

  ### Strings ###

  # end string, must detect escaped characters
  defp tokenize(["\"" | rest], tokens, [:string | context]),
    do: tokenize(rest, [T.string_end() | tokens], context)

  # end indented string
  defp tokenize(["'" | ["'" | rest]], tokens, [:indented_string | context]),
    do: tokenize(rest, [T.indented_string_end() | tokens], context)

  # continue with string content
  defp tokenize([grapheme | rest], tokens, [context | _] = contexts)
       when context in @string_contexts,
       do: tokenize(rest, add_string_content(tokens, grapheme), contexts)

  # start string
  defp tokenize(["\"" | rest], tokens, contexts)
       when not is_string_context(contexts),
       do: tokenize(rest, [T.string_start() | tokens], [:string | contexts])

  # start indented string
  defp tokenize(["'" | ["'" | rest]], tokens, contexts)
       when not is_string_context(contexts),
       do: tokenize(rest, [T.indented_string_start() | tokens], [:indented_string | contexts])

  ### Path Context (expression) ###

  # end path expression context ending with path token and check if it ends with a "/"
  defp tokenize([grapheme | _] = rest, [T.path(path) | tokens], [:path | context])
       when not Grapheme.is_path(grapheme),
       do: tokenize(rest, [upgrade_path_string(path) | tokens], context)

  # continue existing path string
  defp tokenize([grapheme | rest], [T.path(path) | tokens], [:path | _] = context)
       when Grapheme.is_path(grapheme),
       do: tokenize(rest, [T.path(path <> grapheme) | tokens], context)

  # continue existing path expression after interpolation
  defp tokenize(
         [grapheme | rest],
         [T.interpolation_end() | _] = tokens,
         [:path | _] = context
       )
       when Grapheme.is_path(grapheme),
       do: tokenize(rest, [T.path(grapheme) | tokens], context)

  ### Whitespace ###

  # continue whitespace
  defp tokenize([grapheme | rest], [T.whitespace(whitespace) | tokens], context)
       when Grapheme.is_whitespace(grapheme),
       do: tokenize(rest, [T.whitespace(whitespace <> grapheme) | tokens], context)

  # start whitespace
  defp tokenize([grapheme | rest], tokens, context) when Grapheme.is_whitespace(grapheme),
    do: tokenize(rest, [T.whitespace(grapheme) | tokens], context)

  # ellipsis must happen before path
  defp tokenize(["." | ["." | ["." | rest]]], tokens, context),
    do: tokenize(rest, [T.ellipsis() | tokens], context)

  ### Filesystem Paths ###

  # upgrade to URI path if detected
  defp tokenize(["/" | ["/" | rest]], [T.colon() | tokens] = all_tokens, context) do
    with {uri, tokens} <- upgrade_path(tokens, "://"),
         false <- String.contains?(uri, "_") do
      tokenize(rest, [T.uri(uri) | tokens], [:uri | context])
    else
      _ -> tokenize(rest, [T.update() | all_tokens], context)
    end
  end

  # "//" (update) operator here to supercede paths
  defp tokenize(["/" | ["/" | rest]], tokens, context),
    do: tokenize(rest, [T.update() | tokens], context)

  # upgrade to path if needed when "/" is encountered
  defp tokenize(["/" | rest], tokens, context) do
    with {path, upgrade_tokens} <- upgrade_path(tokens, "/") do
      case rest do
        ["$" | ["{" | _]] ->
          tokenize(rest, [T.path(path) | upgrade_tokens], [:path | context])

        [grapheme | _] when Grapheme.is_path(grapheme) ->
          tokenize(rest, [T.path(path) | upgrade_tokens], [:path | context])

        _ ->
          tokenize(rest, [T.divide() | tokens], context)
      end
    else
      _ -> tokenize(rest, [T.divide() | tokens], context)
    end
  end

  # home directory path
  defp tokenize(["~" | ["/" | rest]], tokens, context),
    do: tokenize(rest, [T.path("~/") | tokens], [:path | context])

  # tilde always requires a slash following it
  defp tokenize(["~" | [_ | _] = rest], tokens, context),
    do: tokenize(rest, [T.error("~") | tokens], context)

  ### Store Path ###

  # handle less than or equal operator here to short-circuit
  defp tokenize(["<" | ["=" | rest]], tokens, context),
    do: tokenize(rest, [T.less_than_or_eq() | tokens], context)

  # consume the entire store path if it's a path
  defp tokenize(["<" | [grapheme | rest]], tokens, context) when Grapheme.is_path(grapheme) do
    case Enum.split_while([grapheme | rest], &Grapheme.path?/1) do
      {path, [">" | rest]} -> tokenize(rest, [T.path("<#{path}>") | tokens], context)
      _ -> tokenize([grapheme | rest], [T.less_than() | tokens], context)
    end
  end

  ### Operators ###

  # "<" (less than) operator
  defp tokenize(["<" | rest], tokens, context),
    do: tokenize(rest, [T.less_than() | tokens], context)

  # "+" can be either used for concat or addition
  defp tokenize(["+" | ["+" | rest]], tokens, context),
    do: tokenize(rest, [T.concat() | tokens], context)

  defp tokenize(["+" | rest], tokens, context),
    do: tokenize(rest, [T.add() | tokens], context)

  # "_>" (implication) operator
  defp tokenize(["-" | [">" | rest]], tokens, context),
    do: tokenize(rest, [T.implication() | tokens], context)

  # "-" (subtract) operator
  defp tokenize(["-" | rest], tokens, context),
    do: tokenize(rest, [T.subtract() | tokens], context)

  # "*" (multiply) operator
  defp tokenize(["*" | rest], tokens, context),
    do: tokenize(rest, [T.multiply() | tokens], context)

  defp tokenize(["=" | ["=" | rest]], tokens, context),
    do: tokenize(rest, [T.equal() | tokens], context)

  defp tokenize(["!" | ["=" | rest]], tokens, context),
    do: tokenize(rest, [T.not_equal() | tokens], context)

  defp tokenize([">" | ["=" | rest]], tokens, context),
    do: tokenize(rest, [T.greater_than_or_eq() | tokens], context)

  defp tokenize([">" | rest], tokens, context),
    do: tokenize(rest, [T.greater_than() | tokens], context)

  defp tokenize(["&" | ["&" | rest]], tokens, context),
    do: tokenize(rest, [T.logical_and() | tokens], context)

  defp tokenize(["|" | ["|" | rest]], tokens, context),
    do: tokenize(rest, [T.logical_or() | tokens], context)

  defp tokenize(["!" | rest], tokens, context),
    do: tokenize(rest, [T.invert() | tokens], context)

  defp tokenize(["(" | rest], tokens, context),
    do: tokenize(rest, [T.left_paren() | tokens], context)

  defp tokenize([")" | rest], tokens, context),
    do: tokenize(rest, [T.right_paren() | tokens], context)

  ###  Numbers  ###

  # integer continuation
  defp tokenize([grapheme | rest], [T.integer(integer) | tokens], context)
       when Grapheme.is_decimal(grapheme),
       do: tokenize(rest, [T.integer(integer <> grapheme) | tokens], context)

  # float continuation
  defp tokenize([grapheme | rest], [T.float(float) | tokens], context)
       when Grapheme.is_decimal(grapheme),
       do: tokenize(rest, [T.float(float <> grapheme) | tokens], context)

  # start case for integer
  defp tokenize([grapheme | rest], tokens, context) when Grapheme.is_decimal(grapheme),
    do: tokenize(rest, [T.integer(grapheme) | tokens], context)

  # integer float upgrade
  defp tokenize(["." | rest], [T.integer(integer) | tokens], context),
    do: tokenize(rest, [T.float(integer <> ".") | tokens], context)

  # float scientific notation
  defp tokenize([grapheme | rest], [T.float(float) | tokens], context)
       when grapheme in ["e", "E"] do
    case Enum.split_while(rest, &Grapheme.decimal?/1) do
      {[], rest} ->
        tokenize(rest, [T.error(float <> grapheme) | tokens], context)

      {exponent, rest} ->
        tokenize(rest, [T.float(float <> grapheme <> Enum.join(exponent)) | tokens], context)
    end
  end

  ### Symbols ###

  defp tokenize([";" | rest], tokens, context),
    do: tokenize(rest, [T.semicolon() | tokens], context)

  defp tokenize(["=" | rest], tokens, context),
    do: tokenize(rest, [T.assign() | tokens], context)

  defp tokenize(["@" | rest], tokens, context),
    do: tokenize(rest, [T.at() | tokens], context)

  defp tokenize([":" | rest], tokens, context),
    do: tokenize(rest, [T.colon() | tokens], context)

  defp tokenize(["," | rest], tokens, context),
    do: tokenize(rest, [T.comma() | tokens], context)

  defp tokenize(["." | rest], tokens, context),
    do: tokenize(rest, [T.dot() | tokens], context)

  defp tokenize(["?" | rest], tokens, context),
    do: tokenize(rest, [T.question_mark() | tokens], context)

  defp tokenize(["{" | rest], tokens, context),
    do: tokenize(rest, [T.left_brace() | tokens], context)

  defp tokenize(["}" | rest], tokens, context),
    do: tokenize(rest, [T.right_brace() | tokens], context)

  defp tokenize(["[" | rest], tokens, context),
    do: tokenize(rest, [T.left_bracket() | tokens], context)

  defp tokenize(["]" | rest], tokens, context),
    do: tokenize(rest, [T.right_bracket() | tokens], context)

  ### Keywords ###

  defp tokenize(["l" | ["e" | ["t" | rest]]], tokens, context)
       when is_keyword(rest, tokens),
       do: tokenize(rest, [T.let() | tokens], context)

  defp tokenize(["i" | ["n" | ["h" | ["e" | ["r" | ["i" | ["t" | rest]]]]]]], tokens, context)
       when is_keyword(rest, tokens),
       do: tokenize(rest, [T.inherit() | tokens], context)

  defp tokenize(["i" | ["n" | rest]], tokens, context) when is_keyword(rest, tokens),
    do: tokenize(rest, [T.in() | tokens], context)

  defp tokenize(["t" | ["h" | ["e" | ["n" | rest]]]], tokens, context)
       when is_keyword(rest, tokens),
       do: tokenize(rest, [T.then() | tokens], context)

  defp tokenize(["i" | ["f" | rest]], tokens, context) when is_keyword(rest, tokens),
    do: tokenize(rest, [T.if() | tokens], context)

  defp tokenize(["e" | ["l" | ["s" | ["e" | rest]]]], tokens, context)
       when is_keyword(rest, tokens),
       do: tokenize(rest, [T.else() | tokens], context)

  defp tokenize(["a" | ["s" | ["s" | ["e" | ["r" | ["t" | rest]]]]]], tokens, context)
       when is_keyword(rest, tokens),
       do: tokenize(rest, [T.assert() | tokens], context)

  defp tokenize(["o" | ["r" | rest]], tokens, context) when is_keyword(rest, tokens),
    do: tokenize(rest, [T.or() | tokens], context)

  defp tokenize(["r" | ["e" | ["c" | rest]]], tokens, context) when is_keyword(rest, tokens),
    do: tokenize(rest, [T.rec() | tokens], context)

  defp tokenize(["w" | ["i" | ["t" | ["h" | rest]]]], tokens, context)
       when is_keyword(rest, tokens),
       do: tokenize(rest, [T.with() | tokens], context)

  ### Booleans ###

  defp tokenize(["t" | ["r" | ["u" | ["e" | rest]]]], tokens, context)
       when is_keyword(rest, tokens),
       do: tokenize(rest, [T.boolean("true") | tokens], context)

  defp tokenize(["f" | ["a" | ["l" | ["s" | ["e" | rest]]]]], tokens, context)
       when is_keyword(rest, tokens),
       do: tokenize(rest, [T.boolean("false") | tokens], context)

  ### Identifier ###

  # continue reading identifier
  defp tokenize([grapheme | rest], [T.identifier(_) = token | tokens], context)
       when Grapheme.is_identifier(grapheme),
       do: tokenize(rest, [Token.add(token, grapheme) | tokens], context)

  # base case
  defp tokenize([grapheme | rest], tokens, context) when Grapheme.is_identifier(grapheme),
    do: tokenize(rest, [T.identifier(grapheme) | tokens], context)

  ### Error Fallthrough ###
  # These are the fallthrough cases...they only really occur if encountering  unknown characters.

  # continue error
  defp tokenize([grapheme | rest], [T.error(_) = token | tokens], context),
    do: tokenize(rest, [Token.add(token, grapheme) | tokens], context)

  # begin error
  defp tokenize([grapheme | rest], tokens, context),
    do: tokenize(rest, [T.error(grapheme) | tokens], context)

  ### Helpers ###

  # Adds string content token or appends it
  defp add_string_content([token | tokens], new_content) do
    case token do
      T.string_content(content) ->
        [T.string_content(content <> new_content) | tokens]

      _ ->
        [T.string_content(new_content) | [token | tokens]]
    end
  end

  # The Nix language has the following rule:
  #
  #   At least one slash (/) must appear before any interpolated expression for the result
  #   to be recognized as a path.
  #
  # These functions verify this rule when a path is encountered following an interpolated
  # expression.
  defp upgrade_path([T.interpolation_end() | _], _),
    do: :notpath

  defp upgrade_path([{:token, type, content} | rest], acc)
       when type in @path_tokens,
       do: upgrade_path(rest, content <> acc)

  defp upgrade_path(tokens, acc), do: {acc, tokens}

  defp upgrade_path_string(str) do
    unless String.last(str) == "/" do
      T.path(str)
    else
      T.error(str)
    end
  end
end
