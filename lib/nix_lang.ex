defmodule NixLang do
  @moduledoc """
  Documentation for `NixParser`.
  """

  alias NixLang.{Parser, Tokenizer}

  @doc """
  Parses a Nix file and returns the root node of the parsed tree.  Only returns `:error` if there
  is an OS error reading the file as the parser will return something even if not reading Nix
  source code.
  """
  @spec parse_nix_file(binary()) :: {:ok, Parser.root_node()} | {:error, atom()}
  def parse_nix_file(path), do: with({:ok, nix} <- File.read(path), do: {:ok, parse_nix(nix)})

  @doc """
  Tokenizes then parses a binary string of Nix source code.  Returns the root node of the AST.
  This function should never fail, though it might return an AST of syntax errors.
  """
  @spec parse_nix(binary()) :: Parser.root_node()
  def parse_nix(nix), do: Tokenizer.tokenize(nix) |> Parser.parse()
end
