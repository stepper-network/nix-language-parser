defmodule NixLang.Syntax do
  @moduledoc """
  Syntax definitions within the Nix language.
  """
  defguard element_type(n_or_t) when elem(n_or_t, 0)
  defguard syntax_type(n_or_t) when elem(n_or_t, 1)

  defguard is_element(e)
           when is_tuple(e) and tuple_size(e) == 3 and element_type(e) in [:node, :token]

  defmacro __using__(_opts) do
    quote do
      use NixLang.Syntax.Node
      use NixLang.Syntax.Token
      require NixLang.Syntax
      alias NixLang.Syntax
    end
  end

  def token?(token) when element_type(token) == :token, do: true
  def token?(_), do: false

  def node?(node) when element_type(node) == :node, do: true
  def node?(_), do: false
end
